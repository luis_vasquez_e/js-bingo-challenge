const gridRows = 5;
const valRanges = {
    B:[1,15],
    I:[16,30],
    N:[31,45],
    G:[46,60],
    O:[61,75],
}

let numPool = [...Array(75).keys()].map((value) => value+1);

class BingoCard {
    static getRandomNumber(from, to){
        let selectedNumber = null;
        do {
            selectedNumber = Math.floor(Math.random() * (to - from + 1)) + from;
        } while (numPool[selectedNumber - 1] === -1)
        numPool[selectedNumber - 1] = -1;
        return selectedNumber;
    }

    static generateRandomCard(){
        let numGrid = {B:[], I:[], N:[], G:[], O:[]}

        for (let letter in numGrid){
            for (let j = 0; j < gridRows; j++){
                if (letter === 'N' && j === 2){
                    numGrid[letter].push(true);
                } else {
                    let selectedNumber = BingoCard.getRandomNumber(
                        valRanges[letter][0],
                        valRanges[letter][1]);
                    numGrid[letter].push(selectedNumber);
                }
            }
        }
        return numGrid;
    }
}

/* Display test */
console.log(BingoCard.generateRandomCard());

export default BingoCard;