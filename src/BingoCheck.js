const {BingoCard} = require('./BingoCard')

const gridRows = 5;
const valRanges = {
    B:[1,15],
    I:[16,30],
    N:[31,45],
    G:[46,60],
    O:[61,75],
}

class BingoCheck {
    static checkCall(bingoCard, calledNumbers){
        numGrid = bingoCard.bingoGridValues;
        for (let letter in numGrid){
            for (let j = 0; j < gridRows; j++){
                if (calledNumbers.includes ( numGrid[letter][j]) ){
                    numGrid[letter][j] = true;
                }
            }
        }
    }
}
